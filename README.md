Name: Desmond Wong Qi Jun

Assumptions made:
- Data will be passed in JSON format
- A list of messages will be passed
- Example: [{"sender": "Sender1", "recipient": "Recipient1", "date_created": "19/09/2020", "title": "Title1", "messages": "Hello1"}, 
            {"sender": "Sender2", "recipient": "Recipient2", "date_created": "19/09/2020", "title": "Title2", "messages": "Hello2"},
            {"sender": "Sender3", "recipient": "Recipient3", "date_created": "19/09/2020", "title": "Title3", "messages": "Hello3"}]
- User's name will be passed in parameters of requests

Due to my lack of knowledge in JWT, I did not implement it in this tech test. If I was given a second chance, I would like to spend some time to learn it and try to implement it. 

Thanks for reading