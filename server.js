const express = require('express');
const bodyParser = require('body-parser');

const app = express();


app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


app.post('/ViewMessage', (req, res) => {
    for (let i = 0; i < req.body.length; i++){
        if (req.query.user == req.body[i].recipient){
            if ((req.query.messageTitle == req.body[i].title) && (req.query.messageCreated == req.body[i].date_created) && (req.query.messageSender == req.body[i].sender)){
                    var message ={
                        messages: req.body[i].messages
                    }
                res.send(message);
            }
        }
    }
    res.send("No message!");
})

app.post('/Inbox', (req, res) => {
    var messageList = []
    
    for (let i = 0; i < req.body.length; i++){
        if (req.query.user == req.body[i].recipient){
            messageList.push({
            sender: req.body[i].sender,
            recipient: req.body[i].recipient,
            date_created: req.body[i].date_created,
            title: req.body[i].title,
        })
        }
        
    }
    res.send(messageList) 
})

app.post('/Outbox', (req, res) => {
    var messageList = []
    
    for (let i = 0; i < req.body.length; i++){
        if (req.query.user == req.body[i].sender){
            messageList.push({
            sender: req.body[i].sender,
            recipient: req.body[i].recipient,
            date_created: req.body[i].date_created,
            title: req.body[i].title,
        })
        }
        
    }
    res.send(messageList) 
})



app.listen(3000);